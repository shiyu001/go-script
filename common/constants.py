PRO_SG = "https://gemini.derbysoftsec.com/gproduct"
PRO_US = "https://rush.derbysoftsec.com/gproduct"
PRO_CN = "https://riverdale.derbysoftsec.com/gproduct"

PRO_SG_SYNCER = "https://gemini.derbysoftsec.com/gproduct/redirect.ci?Redirect-URL=http://172.19.229.74:8080/gproduct"

UAT = "https://hulk.derbysoft-test.com/gproduct"

QA = "http://13.112.14.101/gproduct"

LOCAL = "http://127.0.0.1:8080/gproduct"

ENTITIES = ["Hotel", "DistributorHotel", "Source", "Distributor", "Contract", "ThresholdRule", "DistributionRateRule", "HotelPromotion", "PromotionMapping"]

SYNC_ENTITIES = ["hotel", "distributorHotel", "source", "distributor", "contract", "thresholdRule", "distributionRateRule", "hotelPromotion", "promotionMapping"]
