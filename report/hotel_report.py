"""
每天00:00:00对gproduct中的hotel进行扫描
查询：
- 前一天发生变更的酒店数量 {"modifyTime": [2021-05-28T00:00:000Z, 2021-05-29T00:00:000Z]}
- 前一天变更的酒店中，每个供应商有几个 {"modifyTime": [2021-05-28T00:00:000Z, 2021-05-29T00:00:000Z]}
- 前一天变更的酒店中，每个对接有几个
- 前一天新增的酒店数量
- 前一天新增酒店中，每个供应商有几个
- 前一天新增酒店中，每个对接新增了几个
"""

import datetime
import logging
import sys

from common.constants import PRO_SG
from common.query_utils import Constraint, Order, QueryParams
from common.requestor import RpcMethodRequest
from message import sender

logger = logging.getLogger()


def generate_report():
    logger.info("start generate report for hotel")
    request = RpcMethodRequest(PRO_SG)

    current_datetime = datetime.datetime.today()
    yesterday = current_datetime - datetime.timedelta(days=1)
    start_date = yesterday.strftime("%Y-%m-%dT00:00:00.000Z")
    end_date = current_datetime.strftime("%Y-%m-%dT00:00:00.000Z")

    limit = 500
    entities = []
    start_id = None

    query_params = QueryParams("Hotel") \
        .with_constraint(Constraint("createTime", ">=", start_date)) \
        .with_constraint(Constraint("createTime", "<", end_date)) \
        .with_return_properties(["sourceId", "distributorId", "hotelId"]) \
        .with_limit(limit).with_order(Order("id", "asc"))

    while True:
        if start_id:
            query_params.with_constraint(Constraint("id", ">", start_id))

        each_loop = request.call("query.ci", "query", query_params.build_params())

        entities.extend(each_loop)
        if len(each_loop) < limit:
            break
        start_id = each_loop[-1]['id']

    new_entities_count = len(entities)
    group_by_source = {}
    for entity in entities:
        group_by_source.setdefault(entity['sourceId'], []).append(entity)

    report_template = f"{start_date} ~ {end_date} 新增酒店 {new_entities_count} 家\n"

    for source, hotels in group_by_source.items():
        report_template = report_template + f"{source} 新增酒店：{len(hotels)}\n"
        group_by_contract = {}
        for hotel in hotels:
            group_by_contract.setdefault(hotel['distributorId'], []).append(hotel)
        for distributor, contract_hotels in group_by_contract.items():
            report_template = report_template + f"    {source}-{distributor} 新增酒店：{len(contract_hotels)}\n"
    logging.info(report_template)
    sender.send_to_slack(report_template)
