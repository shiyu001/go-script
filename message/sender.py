import requests
import logging

logger = logging.getLogger()


channel_endpoint = "https://hooks.slack.com/services/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"


def send_to_slack(msg, retry_count=3):
    while retry_count > 0:
        retry_count = retry_count - 1
        try:
            payload = {
                "channel": "#go-alarm",
                "username": "GProduct-Sync-Analyser",
                "text": msg,
                "icon_emoji": ":ghost:"
            }
            result = requests.post(channel_endpoint, json=payload)
            if result.status_code == 200:
                return True
        except Exception as e:
            logger.error(e)
    return False
