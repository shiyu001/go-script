import json

from common.constants import PRO_SG, PRO_US, PRO_CN
from product.management import Management
from product.query import Query

import logging

logger = logging.getLogger()


def delete_contract_all_hotels(source_id, distributor_id):
    sg_query = Query(PRO_SG)
    sg_management = Management(PRO_SG)
    us_management = Management(PRO_US)
    cn_management = Management(PRO_CN)
    limit = 100
    last_hotel_id = None
    while True:
        result_set = sg_query.list_contract_hotels(source_id=source_id, distributor_id=distributor_id, start_id=last_hotel_id, limit=limit)
        if len(result_set) <= 0:
            break
        last_hotel_id = result_set[-1]['id']
        ids = [result["id"] for result in result_set]
        logger.info(last_hotel_id, ids)
        sg_management.batch_delete("Hotel", ids)
        us_management.batch_delete("Hotel", ids)
        cn_management.batch_delete("Hotel", ids)
        with open("delete_record.txt", mode="a") as f:
            f.write(json.dumps(ids))
            f.write("\n")
        if len(result_set) < limit:
            logger.info("Request EOF.")
            break


def delete_contract_all_distributor_hotels(source_id, distributor_id):
    sg_query = Query(PRO_SG)
    sg_management = Management(PRO_SG)
    us_management = Management(PRO_US)
    cn_management = Management(PRO_CN)
    limit = 300
    last_hotel_id = None
    while True:
        result_set = sg_query.list_contract_distributor_hotels(source_id=source_id, distributor_id=distributor_id, start_id=last_hotel_id, limit=limit)
        if len(result_set) <= 0:
            break
        last_hotel_id = result_set[-1]['id']
        ids = [result["id"] for result in result_set if result['state'] == 'Deactived']
        logger.info(last_hotel_id, ids)
        sg_management.batch_delete("DistributorHotel", ids)
        us_management.batch_delete("DistributorHotel", ids)
        cn_management.batch_delete("DistributorHotel", ids)
        with open("delete_distributor_record.txt", mode="a") as f:
            f.write(json.dumps(ids))
            f.write("\n")
        if len(result_set) < limit:
            logger.info("Request EOF.")
            break
