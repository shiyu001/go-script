import requests
import json
import logging

logger = logging.getLogger()


class Query(object):
    def __init__(self, endpoint):
        self.endpoint = endpoint

    def get_entity_by_id(self, entity_name, entity_id, retry_count=3):
        while retry_count > 0:
            retry_count = retry_count - 1
            try:
                query_body = {"method": "get", "params": [entity_name, {"id": entity_id}]}
                result = requests.post(self.endpoint + "/management.ci", json=query_body, timeout=300)
                # logger.info("[{}] {}".format(entity_id, self.endpoint + "/management.ci"))
                if result.status_code == 200:
                    if result:
                        entity = json.loads(result.text)['result']
                        return entity
                else:
                    logger.error("ERROR_CODE:", result.status_code)
            except Exception as e:
                logger.error(e)
        return None

    def list_entities(self, entity_name, constraints=None, start_id=None, limit=10, return_properties=None, retry_count=3):
        if constraints is None:
            if start_id:
                constraints = []
        if start_id:
            constraints.append({"entityProperty": "id", "compareOperator": ">", "value": start_id})
        while retry_count > 0:
            retry_count = retry_count - 1
            query_body = {
                "method": "query",
                "params": [
                    entity_name,
                    {
                        "constraints": constraints,
                        "logicOperator": "AND"
                    },
                    {
                        "size": limit,
                        "returnProperties": return_properties,
                        "orders": [
                            {"property": "id", "type": "asc"}
                        ]
                    }
                ]
            }
            result = requests.post(self.endpoint + "/query.ci", json=query_body, timeout=300)
            logger.info("[{}] {}".format(start_id, self.endpoint + "/query.ci"))
            if result.status_code == 200:
                if result:
                    result_set = json.loads(result.text)['result']
                    return result_set
            else:
                logger.error("ERROR_CODE:", result.status_code)
        return []

    def list_contract_hotels(self, source_id, distributor_id, limit=10, start_id=None, retry_count=3):
        """
        offset在服务端使用的是skip()，翻页性能会随着数据量的增加而变差(非常差)
        这里考虑使用id进行翻页(前提是sort必须是按照id进行的)
        """
        while retry_count > 0:
            retry_count = retry_count - 1
            try:
                query_body = {
                    "method": "query",
                    "params": [
                        "Hotel",
                        {
                            "constraints": [
                                {"entityProperty": "sourceId", "compareOperator": "=", "value": source_id},
                                {"entityProperty": "distributorId", "compareOperator": "=", "value": distributor_id}
                            ],
                            "logicOperator": "AND"
                        },
                        {
                            "size": limit,
                            "returnProperties": [
                                "id", "sourceId", "distributorId", "state"
                            ],
                            "orders": [{"property": "id", "type": "asc"}]
                        }
                    ]
                }
                if start_id:
                    query_body['params'][1]['constraints'].append({"entityProperty": "id", "compareOperator": ">", "value": start_id})
                result = requests.post(self.endpoint + "/query.ci", json=query_body, timeout=300)
                logger.info(query_body)
                logger.info("[{}] {}".format(start_id, self.endpoint + "/query.ci"))
                if result.status_code == 200:
                    if result:
                        result_set = json.loads(result.text)['result']
                        return result_set
                else:
                    logger.error("ERROR_CODE:", result.status_code)
            except Exception as e:
                logger.error(e)
        return []

    def list_contract_distributor_hotels(self, source_id, distributor_id, start_id=None, limit=10, retry_count=3):
        while retry_count > 0:
            retry_count = retry_count - 1
            try:
                query_body = {
                    "method": "query",
                    "params": [
                        "DistributorHotel",
                        {
                            "constraints": [
                                {"entityProperty": "sourceId", "compareOperator": "=", "value": source_id},
                                {"entityProperty": "distributorId", "compareOperator": "=", "value": distributor_id}
                            ],
                            "logicOperator": "AND"
                        },
                        {
                            "size": limit,
                            "returnProperties":
                                ["id", "sourceId", "distributorId", "state"],
                            "orders": [{"property": "id", "type": "asc"}]
                        }
                    ]
                }
                if start_id:
                    query_body['params'][1]['constraints'].append({"entityProperty": "id", "compareOperator": ">", "value": start_id})
                result = requests.post(self.endpoint + "/query.ci", json=query_body, timeout=300)
                logger.info(query_body)
                logger.info("[{}] {}".format(start_id, self.endpoint + "/query.ci"))
                if result.status_code == 200:
                    if result:
                        result_set = json.loads(result.text)['result']
                        return result_set
                else:
                    logger.error("ERROR_CODE:", result.status_code)
            except Exception as e:
                logger.error(e)
        return []
