import requests
import json

CCS_ARI = "ccs!/go_save?topic=dstorage_hosts&queryKey=key"
CCS_CDS = "ccs!/go_cds?topic=dstorage_hosts&queryKey=key"

ENV_UAT = "https://hulk.derbysoft-test.com/gproduct/redirect.ci?Redirect-URL=http://10.110.206.196:8080/gproduct"
KEY_DSWITCH_CDS = "dswitch_cds"
KEY_DSWITCH_ARI = "dswitch_ari"
KEY_ENDPOINT = "endpoint"

IGNORE_DISTRIBUTORS = ["EXPEDIA"]


def query_distributor_by_id(distributor_id):
    if not distributor_id:
        return None
    query_body = {"method": "get", "params": ["Distributor", {"id": distributor_id}]}
    result = requests.post(ENV_UAT + "/management.ci", json=query_body)
    if result.status_code == 200:
        if result:
            distributor = json.loads(result.text)['result']
            return distributor
    else:
        print("ERROR_CODE:", result.status_code)
    return None


def query_distributors_only_endpoints(distributor_id=None):
    if distributor_id:
        query_body = {
            "method": "query",
            "params": [
                "Distributor",
                {
                    "entityProperty": "id",
                    "compareOperator": "=",
                    "value": distributor_id
                },
                {
                    "returnProperties": [
                        "id",
                        "endpoints"
                    ],
                    "orders": [
                        {
                            "property": "id",
                            "type": "asc"
                        }
                    ],
                    "startIndex": 0
                }
            ]
        }
    else:
        query_body = {
            "method": "query",
            "params": [
                "Distributor",
                {
                    "entityProperty": "id",
                    "compareOperator": ">",
                    "value": "0"
                },
                {
                    "returnProperties": [
                        "id",
                        "endpoints"
                    ],
                    "orders": [
                        {
                            "property": "id",
                            "type": "asc"
                        }
                    ],
                    "startIndex": 0
                }
            ]
        }
    result = requests.post(ENV_UAT + "/query.ci", json=query_body)
    query_result = result.text
    json_result = json.loads(query_result)
    distributors = json_result['result']
    print("Query %d distributors" % len(distributors))
    distributors = [distributor for distributor in distributors if
                    KEY_DSWITCH_ARI in distributor['endpoints'] or
                    KEY_DSWITCH_CDS in distributor['endpoints']]
    print("There are %d distributors has dswitch_ari|dswitch_cds" % len(distributors))
    return distributors


def process_endpoints(distributor_id, endpoints):
    print("[%s] The original endpoints: %s" % (distributor_id, endpoints))
    dswitch_ari_endpoint_origin = ""
    dswitch_cds_endpoint_origin = ""
    if KEY_DSWITCH_ARI in endpoints and KEY_ENDPOINT in endpoints[KEY_DSWITCH_ARI]:
        dswitch_ari_endpoint_origin = endpoints[KEY_DSWITCH_ARI][KEY_ENDPOINT]
        endpoints[KEY_DSWITCH_ARI][KEY_ENDPOINT] = CCS_ARI
    if KEY_DSWITCH_CDS in endpoints and KEY_ENDPOINT in endpoints[KEY_DSWITCH_CDS]:
        dswitch_cds_endpoint_origin = endpoints[KEY_DSWITCH_CDS][KEY_ENDPOINT]
        endpoints[KEY_DSWITCH_CDS][KEY_ENDPOINT] = CCS_CDS
    print("[%s] After process endpoints: %s" % (distributor_id, endpoints))
    return distributor_id, endpoints, dswitch_ari_endpoint_origin, dswitch_cds_endpoint_origin


def update_distributor_endpoints(distributor_id, endpoints):
    distributor = query_distributor_by_id(distributor_id)
    if distributor:
        distributor['modifyTime'] = None
        distributor['createTime'] = None
        distributor['endpoints'] = endpoints
        update_body = {
            "method": "put",
            "params": ["Distributor", distributor]
        }
        result = requests.post(ENV_UAT + "/management.ci", json=update_body)
        if result.status_code == 200:
            print("After update: ", result)
            return True
    return False


def record_history(distributor_id, dswitch_ari_endpoint_origin, dswitch_cds_endpoint_origin):
    with open("update_record.txt", mode="a") as file:
        record = {"distributor_id": distributor_id,
                  "dswitch_ari_endpoint_origin": dswitch_ari_endpoint_origin,
                  "dswitch_cds_endpoint_origin": dswitch_cds_endpoint_origin}
        file.write(json.dumps(record))
        file.write("\n")


def __main():
    distributors = query_distributors_only_endpoints(distributor_id="")
    size = len(distributors)
    for distributor in distributors:
        print("LEFT: ", size)
        size = size - 1
        distributor_id = distributor["id"]
        if distributor_id in IGNORE_DISTRIBUTORS:
            print("Ignored:", distributor_id)
            continue
        distributor_endpoints = distributor["endpoints"]
        distributor_id, distributor_endpoints, dswitch_ari_endpoint_origin, dswitch_cds_endpoint_origin = process_endpoints(distributor_id, distributor_endpoints)
        update_success = update_distributor_endpoints(distributor_id, distributor_endpoints)
        if update_success:
            record_history(distributor_id, dswitch_ari_endpoint_origin, dswitch_cds_endpoint_origin)


if __name__ == '__main__':
    __main()
