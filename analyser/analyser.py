from product.query import Query
from common.constants import PRO_SG, PRO_CN, PRO_US
from datetime import datetime
import threading
import logging

logger = logging.getLogger()


class Analyser(object):
    def __init__(self, entity_name, constraints=None):
        self.sg_query = Query(PRO_SG)
        self.us_query = Query(PRO_US)
        self.cn_query = Query(PRO_CN)
        self.entity_name = entity_name
        self.constraints = constraints

        self.analyse_result = {"entity_name": entity_name, "start_time": datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"), "results": []}
        self.limit = 30
        self.start_id = None
        self.shark_count = 1
        self.shark_size = self.limit * 100
        self.shard_finished_listeners = []
        self.err_count = 10
        self.err_count_threshold = 0

    def reset_analyse_result(self):
        self.analyse_result = {"entity_name": self.entity_name, "start_time": datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"), "results": []}

    def worker(self, sg_entity_base):
        entity_id = sg_entity_base['id']
        # entity_sg = self.sg_query.get_entity_by_id(entity_name=self.entity_name, entity_id=entity_id)
        entity_us = self.us_query.get_entity_by_id(entity_name=self.entity_name, entity_id=entity_id)
        entity_cn = self.cn_query.get_entity_by_id(entity_name=self.entity_name, entity_id=entity_id)
        modify_time_sg = sg_entity_base['modifyTime']
        modify_time_us = entity_us['modifyTime'] if entity_us else None
        modify_time_cn = entity_cn['modifyTime'] if entity_cn else None
        us_need_sync = modify_time_us != modify_time_sg
        cn_need_sync = modify_time_cn != modify_time_sg
        print("{:>60} | {} | {}({}) | {}({})".format(entity_id, modify_time_sg, modify_time_us, us_need_sync, modify_time_cn, cn_need_sync))
        if us_need_sync or cn_need_sync:
            temp_record = {'entity_name': self.entity_name, 'entity_id': entity_id, 'modify_time_sg': modify_time_sg,
                           'modify_time_us': modify_time_us, 'modify_time_cn': modify_time_cn}
            self.analyse_result['results'].append(temp_record)

    def analyse(self):
        print(
            "{:>60} | {:24} | {:31} | {:31}".format("entity id", "modify time in sg", "modify time in us", "modify time in cn"))
        count = 0
        while True:
            try:
                result_set = self.sg_query.list_entities(self.entity_name, constraints=self.constraints, start_id=self.start_id, limit=self.limit,
                                                         return_properties=['id', 'modifyTime'])
                if len(result_set) <= 0:
                    logger.info("Request EOF.")
                    break
                self.start_id = result_set[-1]['id']

                tasks = []
                for result in result_set:
                    count = count + 1
                    t = threading.Thread(target=self.worker, args=(result,))
                    t.start()
                    tasks.append(t)

                for task in tasks:
                    task.join()

                if count >= self.shark_size:
                    self.analyse_result["shark_size"] = count
                    count = 0
                    self.on_shark_finished()
                    self.shark_count += 1

                if len(result_set) < self.limit:
                    logger.info("Request EOF.")
                    break
            except Exception as e:
                logger.error(e)
                self.err_count_threshold = self.err_count_threshold + 1
                if self.err_count > self.err_count_threshold:
                    logger.error("Error count %s > %s, stop!")
                    break
                continue
        self.analyse_result["shark_size"] = count
        self.on_shark_finished()

    def on_shark_finished(self):
        self.analyse_result["shark_id"] = "SHARK-{}-{}".format(self.entity_name, self.shark_count)
        self.analyse_result["end_time"] = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        if self.shard_finished_listeners:
            for listener in self.shard_finished_listeners:
                listener.on_shark_finished(self.analyse_result)
        self.reset_analyse_result()

    def register_shark_finished_listener(self, listener):
        if listener:
            self.shard_finished_listeners.append(listener)
