import json
import sys

import requests
import logging

logger = logging.getLogger()


class RpcMethodRequest(object):
    """
    用于访问组件暴漏的rpcMethod
    """

    def __init__(self, server_endpoint):
        self.server_endpoint = server_endpoint

    def call(self, rpc_path, method, params):
        endpoint = self.server_endpoint + '/' + rpc_path
        request_body = {
            "method": method,
            "params": params
        }
        stream_req = f"RQ >> {endpoint} POST {json.dumps(request_body)}"
        logger.info(stream_req)
        result = requests.post(endpoint, json=request_body, timeout=300)
        stream_res = f"RS << {endpoint} POST {result.text}"
        logger.info(stream_res)

        if result.status_code != 200:
            # TODO throw ex
            return None
        result_set = json.loads(result.text)['result']
        return result_set


def __test():
    rpc_method_request = RpcMethodRequest("https://support.derbysoftsec.com/s3gate")
    params = [
        "/go/certification/uat/model/job/4339B5FC626449D1B26A1E37ED014EBE"
    ]
    result = rpc_method_request.call(rpc_path="s3gate.rpc", method="listFiles", params=params)
    print(result)


if __name__ == '__main__':
    __test()
