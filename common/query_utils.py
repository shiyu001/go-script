import json
import logging

logger = logging.getLogger()


class Constraint(object):
    def __init__(self, field, op, val):
        self.field = field
        self.op = op
        self.val = val


class Order(object):
    def __init__(self, field, order):
        self.field = field
        self.order = order


class QueryParams(object):

    def __init__(self, entity):
        self.entity = entity
        self.constraints = []
        self.logic_op = "AND"
        self.limit = 10
        self.return_properties = None
        self.orders = []
        self.start_idx = 0

    def with_entity(self, entity):
        self.entity = entity
        return self

    def with_constraint(self, constraint):
        self.constraints.append(constraint)
        return self

    def with_limit(self, limit):
        self.limit = limit
        return self

    def with_return_properties(self, return_properties):
        self.return_properties = return_properties
        return self

    def with_start_idx(self, start_idx):
        self.start_idx = start_idx
        return self

    def with_order(self, order):
        self.orders.append(order)
        return self

    def build_json_params(self):
        return json.dumps(self.build_params())

    def build_params(self):
        param_0 = self.entity
        param_1 = {"constraints": [], "logicOperator": self.logic_op}
        for constraint in self.constraints:
            param_1["constraints"].append({
                "entityProperty": constraint.field,
                "compareOperator": constraint.op,
                "value": constraint.val
            })
        param_2 = {
            "size": self.limit,
            "returnProperties": self.return_properties,
            "orders": [],
            "startIndex": self.start_idx
        }
        for order in self.orders:
            param_2["orders"].append({
                "property": order.field,
                "type": order.order
            })

        params = [param_0, param_1, param_2]
        return params


def __test():
    params = QueryParams("Hotel") \
        .with_constraint(Constraint("sourceId", "=", "IHG")) \
        .with_constraint(Constraint("distributorId", "=", "SUNHOTELS")) \
        .with_limit(100) \
        .with_return_properties(None) \
        .with_order(Order("id", "asc")) \
        .with_start_idx(0).build_json_params()

    logger.info(params)


if __name__ == '__main__':
    __test()
