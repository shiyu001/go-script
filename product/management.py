import requests
import logging

logger = logging.getLogger()


class Management(object):
    def __init__(self, endpoint):
        self.endpoint = endpoint

    def batch_delete(self, entity_name, entity_ids, retry_count=3):
        while retry_count > 0:
            retry_count = retry_count - 1
            try:
                query_body = {"method": "batchDelete", "params": [entity_name, [{"id": entity_id} for entity_id in entity_ids]]}
                result = requests.post(self.endpoint + "/management.ci", json=query_body)
                if result.status_code == 200:
                    logger.info("DELETE successful: ", result)
                    return
                else:
                    logger.info("ERROR_CODE:", result.status_code)
            except Exception as e:
                logger.info(e)
