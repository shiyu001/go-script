import argparse
import sys

from analyser.analyser import Analyser
from common.constants import *
from message import sender
from product.sync import Synchronizer
from service import delete
from report import hotel_report
import logging
import logging.config

logger = logging.getLogger()


def chunks(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


class Listener(object):
    @staticmethod
    def on_shark_finished(analyse_result):
        shark_id = analyse_result['shark_id']
        shark_size = analyse_result['shark_size']
        entity_name = analyse_result['entity_name']
        results = analyse_result['results']
        if len(results) <= 0:
            logger.info("Need to sync count is 0, do not send robot message.")
            return
        msg = ""
        msg += "{:20}: [{}]\n".format("Shark-ID", shark_id)
        msg += "{:20}: [{}]\n".format("Shark-Size", shark_size)
        msg += "{:20}: [{}]\n".format("Entity", entity_name)
        msg += "{:20}: [{}]\n".format("Need to sync count:", len(results))
        msg += "{:20}: [{}]\n".format("Analyse Start Time:", analyse_result['start_time'])
        msg += "{:20}: [{}]\n".format("Analyse End Time:", analyse_result['end_time'])
        # if results:
        #     msg = msg + "{:>5} | {:>60} | {:24} | {:24} | {:24}\n".format("NO", "ID", "modify_time(SG)",
        #                                                                   "modify_time(US)",
        #                                                                   "modify_time(CN)")
        #     for i in range(len(results)):
        #         r = results[i]
        #         entity_id = r['entity_id']
        #         modify_time_sg = r['modify_time_sg']
        #         modify_time_us = r['modify_time_us']
        #         modify_time_cn = r['modify_time_cn']
        #         msg = msg + "{:5} | {:>60} | {:24} | {:24} | {:24}\n".format(i + 1, entity_id if entity_id else 'null',
        #                                                                      modify_time_sg if modify_time_sg else 'null',
        #                                                                      modify_time_us if modify_time_us else 'null',
        #                                                                      modify_time_cn if modify_time_cn else 'null')
        # else:
        #     msg = msg + "Entity sync status good!\n"
        logger.info(msg)
        sender.send_to_slack(msg)
        ids = [entity['entity_id'] for entity in results]
        if ids:
            logger.info("start sync...")
            sync_us = Synchronizer(PRO_US)
            sync_cn = Synchronizer(PRO_CN)
            hump_name = entity_name[0].lower() + entity_name[1:]

            result_chunks = list(chunks(ids, 10))
            for ids_chunk in result_chunks:
                logger.info("ids_chunk: {}".format(ids_chunk))
                sync_us.sync(entity_name=hump_name, entity_ids=ids_chunk)
                sync_cn.sync(entity_name=hump_name, entity_ids=ids_chunk)


def start_analyse(collection, constraints):
    analyser = Analyser(collection, constraints=constraints)
    analyser.register_shark_finished_listener(listener=Listener())
    analyser.analyse()


def command_sync(args):
    collection = args.collection
    source_id = args.sourceId
    distributor_id = args.distributorId
    _all = args.all
    constraints = []
    if collection in ['Hotel', 'DistributorHotel']:
        if _all == 'True':
            pass
        else:
            if not source_id or not distributor_id:
                print("[错误] 必须指定 sourceId 和 distributorId !")
                sys.exit(-1)
            constraints = [
                {"entityProperty": "sourceId", "compareOperator": "=", "value": source_id},
                {"entityProperty": "distributorId", "compareOperator": "=", "value": distributor_id}
            ]
    start_analyse(collection, constraints)


def command_menu():
    print("Welcome! Select the entity you want to sync: ")
    for i in range(len(ENTITIES)):
        print("[{}] - {}".format(i + 1, ENTITIES[i]))
    print()
    print("[{}] - {}".format(0, "EXIT!"))
    entity = None
    while True:
        entity_idx = input("Input No. and confirm with ENTER: ")
        try:
            entity_idx = int(entity_idx)
            if entity_idx == 0:
                print("Bye!!!")
                sys.exit(0)
            if entity_idx - 1 not in range(len(ENTITIES)):
                continue
            entity = ENTITIES[entity_idx - 1]
        except Exception as e:
            continue
        break
    source_id = None
    distributor_id = None
    constraints = []
    if entity in ['Hotel', 'DistributorHotel']:
        while not source_id:
            source_id = input("Input SourceId and confirm with ENTER: ")
        while not distributor_id:
            distributor_id = input("Input DistributorId and confirm with ENTER: ")
        constraints = [
            {"entityProperty": "sourceId", "compareOperator": "=", "value": source_id},
            {"entityProperty": "distributorId", "compareOperator": "=", "value": distributor_id}
        ]
    start_analyse(entity, constraints)


def command_report():
    hotel_report.generate_report()


def command_delete(args):
    collection = args.collection
    if collection not in ['Hotel', 'DistributorHotel']:
        print('[错误] 不支持的表: [%s]' % collection)
        sys.exit(-1)
    source_id = args.sourceId
    distributor_id = args.distributorId
    if not source_id or not distributor_id:
        print("[错误] 必须指定 sourceId 和 distributorId !")
        sys.exit(-1)
    confirm = input('确认删除[sourceId=%s], [distributorId=%s]的Hotel信息吗？(Y/N)' % (source_id, distributor_id))
    if confirm.upper() != 'Y':
        print('取消删除！')
        sys.exit(0)
    if collection == 'Hotel':
        delete.delete_contract_all_hotels(source_id=source_id, distributor_id=distributor_id)
    if collection == 'DistributorHotel':
        delete.delete_contract_all_distributor_hotels(source_id=source_id, distributor_id=distributor_id)


def init_logger():
    logging.config.fileConfig('logging.conf')
    logging.basicConfig(level=logging.INFO)


def __main():
    init_logger()
    parser = argparse.ArgumentParser(description="Go Utils powered by python3.")
    # parser.add_argument('-s', '--sync', help="多区同步", action='store_true')
    sub_parser = parser.add_subparsers(title='Available subcommands', dest='command')
    parser_sync = sub_parser.add_parser('sync', help='多区同步')
    parser_sync.add_argument('--collection', help='表名', required=True)
    parser_sync.add_argument('--sourceId', help='供应商')
    parser_sync.add_argument('--distributorId', help='渠道')
    parser_sync.add_argument('--all', help='开关，全表同步 [True or False]')
    parser_menu = sub_parser.add_parser('menu', help='菜单交互')
    parser_delete = sub_parser.add_parser('delete', help='删除数据，慎用！！！')
    parser_delete.add_argument('--collection', help='表名，目前仅支持hotel和distributorHotel', required=True)
    parser_delete.add_argument('--sourceId', help='供应商')
    parser_delete.add_argument('--distributorId', help='渠道')

    parser_report = sub_parser.add_parser('report', help='生成报告')

    args = parser.parse_args()

    if args.command == 'sync':
        command_sync(args)
    elif args.command == 'menu':
        command_menu()
    elif args.command == 'delete':
        command_delete(args)
    elif args.command == 'report':
        command_report()
    else:
        return


if __name__ == '__main__':
    __main()
