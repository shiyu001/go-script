import requests
from common.constants import PRO_US
from common.constants import PRO_CN
import logging

logger = logging.getLogger()

ids = [""]


class Synchronizer(object):

    def __init__(self, endpoint):
        self.endpoint = endpoint

    def sync(self, entity_name, entity_ids, retry_count=3):
        while retry_count > 0:
            try:
                retry_count = retry_count - 1
                payload = {"method": "syncByIds", "params": [entity_name, entity_ids]}
                result = requests.post(self.endpoint + "/sync.rpc", json=payload)
                if result.status_code == 200:
                    return True
            except Exception as e:
                logger.error(e)
        return False


def chunks(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


if __name__ == '__main__':
    sync_us = Synchronizer(PRO_US)
    sync_cn = Synchronizer(PRO_CN)
    result_chunks = list(chunks(ids, 10))
    for ids_chunk in result_chunks:
        logger.info("ids_chunk: {}".format(ids_chunk))
        sync_us.sync(entity_name="distributorHotel", entity_ids=ids_chunk)
        sync_cn.sync(entity_name="distributorHotel", entity_ids=ids_chunk)
