command="python3 main.py sync --collection ${collectionName}"

[ ! -z ${sourceId} ] && command="${command} --sourceId ${sourceId}"

[ ! -z ${distributorId} ] && command="${command} --distributorId ${distributorId}"

[ ! -z ${all} ] && command="${command} --all ${all}"


$command
