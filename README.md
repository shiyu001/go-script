# go-script

## Getting start

### 环境

- linux/windows/mac
- python3.6+

```
# install depency
pip3 install requests
```

## menu命令

`python3 main.py menu`

**menu**会提供一个可交互的菜单

## sync命令

**sync**用于执行多区数据同步，可以指定表名来进行全量同步，对于`hotel`和`distributorHotel`，还可以通过指定`sourceId`和`distributorId`来同步对接级别的数据

### 同步原理

- `main.py` 分析`gproduct`多区数据同步的状态，比较`modifyTime`，并最终汇总成为报告，发送到`slack` `channel #go-alarm`中

由于Hotel和DistributorHotel数量巨大，分析非常缓慢，目前并发量为30，请酌情调整，调整方式为：修改`analyser.py`中`analyse`方法的`limit`大小

```
# 查看帮助
python3 main.py sync -h

# 同步IHG-ATI的所有供应商酒店信息
python3 main.py sync --collection hotel --sourceId IHG --distributorId ATI  

# 同步IHG-ATI的所有渠道酒店信息
python3 main.py sync --collection distributorHotel --sourceId IHG --distributorId ATI  

# 同步供应商信息
python3 main.py sync --collection source

# 同步渠道信息
python3 main.py sync --collection distributor

# 同步对接信息
python3 main.py sync --collection contract
```

## delete命令

**delete**命令用于删除三区的数据，该命令是作用于**生产环境**，物理删除，请慎用。目前仅支持`hotel`和`distributorHotel`对接级别的删除

```
# 删除供应商酒店
python3 main.py delete --collection hotel --sourceId abc distributorId efg
# 删除渠道酒店
python3 main.py delete --collection distributorHotel --sourceId abc distributorId efg
```

## TODO-LIST

- 目前只能生成报告，不能自动触发同步接口
